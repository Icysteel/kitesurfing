import { dropdownMenuOnClickEventHandler } from './dropdownMenuEventHandler.js';
import { logoOnClickEventHandler } from './logoEventHandler.js';
import { forUsButtonOnClickEventHandler } from './forUsButtonEventHandler.js';

const eventHandler = function() {
  logoOnClickEventHandler();
  dropdownMenuOnClickEventHandler();
  forUsButtonOnClickEventHandler();
};

export {
  eventHandler,
};