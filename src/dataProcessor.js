const getForecastData = function(data) {
  const pureData = ((el) => {
    return (el = el.substring(0, el.length - 1));
  });

  const getWindDirection = (el) => {
    const w = el.split('/');
    return w[0].slice(0, w[0].search(/\d/));
  };

  const getWindSpeed = (el) => {
    const w = el.split('/');
    return +w[0].replace(/[A-Z]+/, '');
  };

  const getWindGust = (el) => {
    const w = el.split('/');
    return +w[1];
  };

  const finalResult = data
      .replace(/\\\\n/g, '')
      .replace(/<small>/g, '')
      .replace(/<\/small>/g, '')
      .replace(/<br\/>/g, '@')
      .replace(/]\[/g, '')
      .replace(/\[/g, '')
      .replace(/\"/g, '')
      .replace(/,/g, '')
      .split('@')
      .filter((el) => {
        return (el[2] === ' ');
      }).map((element) => {
        const el = element.split(' ');
        return el;
      }).map((el) => ({
        date: pureData(el[1]),
        hour: pureData(el[2]),
        windSpeed: getWindSpeed(el[3]),
        windGust: getWindGust(el[3]),
        windDirection: getWindDirection(el[3]),
        temp: pureData(el[4]),
        rain: el[6],
        cloudy: el[5],
      }));
  return finalResult;
};

export { getForecastData };