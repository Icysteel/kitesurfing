import * as $ from 'jquery';
import { locations } from './database.js';

const appendSpots = () => {
  Array.from(locations).forEach((el) => {
    $('.dropdown-menu').append($(`<div class="item" id="${el.id}">${el.place}</div>`));
  });
};

export { appendSpots };