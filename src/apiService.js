import * as $ from 'jquery';
import { getForecastData } from './dataProcessor.js';
import { buildForecastTable } from './forecastTableBuilder.js';
import { getDates } from './dateGenerator.js';
import {
  buildHourlyDataColumn,
  buildWrapperForAllForecastData,
  buildLoaderDiv,
  windDirectionArrow,
} from './forecastTableBuilder.js';

const callApi = () => {
  $('.item').click(function() {
    const spotId = $(this).attr('id');

    buildForecastTable();

    const hours = 172 - new Date().getHours();
    const url =
    'http://allorigins.me/get?url=' +
    encodeURIComponent(
        `http://wap.windguru.cz/view.php?sc=${spotId}&wj=msd&tj=c&odh=0&doh=24&fhours=${hours}&m=3`
    ) +
      '&callback=?';

    const getData = new Promise(function(resolve) {
      buildLoaderDiv();

      const days = getDates();

      const forecastPerDay = [];

      $.get(url, function(response) {
        getForecastData(
            JSON.stringify(/<small>(.*?)<\/small>/g.exec(response)[0])
        )
            .map((el) => {
              return Object.values(el);
            })
            .forEach((el) => {
              if (forecastPerDay[el[0]] === undefined) {
                forecastPerDay[el[0]] = [];
              }
              forecastPerDay[el[0]].push(el);
            });

        forecastPerDay[days[0]].forEach((el) => {
          buildHourlyDataColumn(
              el[1].replace('h', 'ч'),
              el[2],
              el[3],
              el[4],
              el[5],
              el[6] || 0,
              el[7] || 0
          );
        });

        const compass = Array.from(
            document.getElementsByClassName('square-field compass')
        );

        windDirectionArrow(compass);

        const navLink = Array.from($('.nav-link')).forEach((link, index) => {
          link.onclick = function() {
            if ($('.all-forecast-data-wrapper')) {
              $('.all-forecast-data-wrapper').remove();
            }

            buildWrapperForAllForecastData();

            forecastPerDay[days[index]].forEach((el) => {
              buildHourlyDataColumn(
                  el[1].replace('h', 'ч'),
                  el[2],
                  el[3],
                  el[4],
                  el[5],
                  el[6] || 0,
                  el[7] || 0
              );

              const compass = Array.from(
                  document.getElementsByClassName('square-field compass')
              );

              windDirectionArrow(compass);
            });
          };
        });
        resolve(null);
      }).done(function() {
        $('.loader').hide();
      });
    });
  });
};

export { callApi };