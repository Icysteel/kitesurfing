import * as $ from 'jquery';

const logoOnClickEventHandler = function() {
  $('.navbar-logo').on('click', function() {
    if ($('.header6').is(':hidden')) {
      $('.header6').show();
    }

    if ($('#page-container').is(':visible')) {
      $('#page-container').hide();
    }

    if ($('.for-us').is(':visible')) {
      $('.for-us').hide();
    }

    if ($('.dropdown-navbar').is(':visible')) {
      $('.dropdown-navbar').hide();
    }
  });
};

export {
  logoOnClickEventHandler,
};