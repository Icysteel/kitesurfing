import * as $ from 'jquery';

const forUsButtonOnClickEventHandler = function() {
  $('#btn-for-us').on('click', function() {
    if ($('.for-us').is(':hidden')) {
      $('.for-us').show();
    }

    if ($('div.ui.selection.dropdown.dropdown-navbar').is(':hidden')) {
      $('div.ui.selection.dropdown.dropdown-navbar').show();
    }

    if ($('.header6').is(':visible')) {
      $('.header6').hide();
    }

    if ($('#page-container').is(':visible')) {
      $('#page-container').hide();
    }
  });
};

export {
  forUsButtonOnClickEventHandler,
};