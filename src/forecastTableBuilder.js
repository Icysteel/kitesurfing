import * as $ from 'jquery';
import { getDates } from './dateGenerator';

const createDailyForecastDataWrapper = () => {
  if (!$('.daily-forecast-data-wrapper')[0]) {
    $('<div></div>')
        .addClass('daily-forecast-data-wrapper')
        .appendTo('.tab-content');
  }
};

const createDailyDataLabelWrapper = () => {
  if (!$('.daily-data-label-wrapper')[0]) {
    $('<div></div>')
        .addClass('daily-data-label-wrapper')
        .appendTo('.daily-forecast-data-wrapper');
  }
};

const buildLabelArea = (label) => {
  if (label) {
    $('<div></div>')
        .addClass('data-provided-label')
        .appendTo('.daily-data-label-wrapper')
        .text(label);
  } else {
    $('<div></div>')
        .addClass('empty-data-provided-label')
        .appendTo('.daily-data-label-wrapper');
  }
};

const buildWrapperForAllForecastData = (label) => {
  $('<div></div>')
      .addClass('all-forecast-data-wrapper')
      .appendTo('.daily-forecast-data-wrapper');
};

const backgroundColorWindFieldSetter = (
    currentTag,
    data,
    additionalCSSclass
) => {
  if (+data <= 2) {
    currentTag.addClass(`${additionalCSSclass}-2`);
  } else if (+data >= 25) {
    currentTag.addClass(`${additionalCSSclass}-25`);
  } else {
    currentTag.addClass(`${additionalCSSclass}-${Math.round(+data)}`);
  }
  return currentTag;
};

const backgroundColorTemperatureFieldSetter = (
    currentTag,
    data,
    additionalCSSclass
) => {
  if (+data <= 2) {
    currentTag.addClass(`${additionalCSSclass}-2`);
  } else if (+data >= 32) {
    currentTag.addClass(`${additionalCSSclass}-32`);
  } else {
    currentTag.addClass(`${additionalCSSclass}-${Math.round(+data)}`);
  }
  return currentTag;
};

const buildASquareField = (data, additionalCSSclass) => {
  if (data === '-') {
    const $square = $('<div> </div>').addClass(`past-forecast`);
    return $square;
  }
  let $square = $('<div>' + data + '</div>').addClass('square-field');
  if (additionalCSSclass === 'wind-speed-formatter') {
    $square = backgroundColorWindFieldSetter($square, data, additionalCSSclass);
  } else if (additionalCSSclass === 'temperature-field-backgroundColor') {
    $square = backgroundColorTemperatureFieldSetter(
        $square,
        data,
        additionalCSSclass
    );
  } else if (additionalCSSclass !== '') {
    $square.addClass(`${additionalCSSclass}`);
  }
  return $square;
};

const forecastRatingBuilder = (wind) => {
  const windSpeed = +wind;
  if (wind === '-') {
    return 'past-forecast';
  } else if (windSpeed < 7) {
    return 'rating-formatter-0';
  } else if (windSpeed >= 7 && windSpeed <= 9.5) {
    return 'rating-formatter-1';
  } else if (windSpeed > 9.5 && windSpeed <= 11.5) {
    return 'rating-formatter-2';
  } else if (windSpeed > 11.5) {
    return 'rating-formatter-3';
  }

  return {};
};
const weatherIconsinitialization = (cloudsData, chanceOfRainData) => {
  const clouds = cloudsData.toString().replace('%', '');
  const chanceOfRain = chanceOfRainData.toString().replace('mm', '');
  if (clouds === '-') {
    return 'past-forecast';
  } else if (clouds < 20) {
    return 'weather-icon-sun';
  } else if (clouds >= 20 && clouds <= 70 && chanceOfRain < 10) {
    return 'weather-icon-sun-clouds';
  } else if (clouds >= 20 && clouds <= 70 && chanceOfRain >= 10) {
    return 'weather-icon-sun-clouds-rain';
  } else if (clouds > 70 && chanceOfRain < 10) {
    return 'weather-icon-mixed-clouds';
  } else if (clouds > 70 && chanceOfRain > 50) {
    return 'weather-icon-clouds-rain';
  }

  return {};
};

const buildLabelDataColumn = () => {
  if (!$('.daily-forecast-data-wrapper')[0]) {
    createDailyForecastDataWrapper();
  }

  if (!$('.daily-data-label-wrapper')[0]) {
    createDailyDataLabelWrapper();
  }

  buildLabelArea('');
  buildLabelArea('Скорост (м/с)');
  buildLabelArea('Пориви (м/с)');
  buildLabelArea('Посока на вятъра');
  buildLabelArea('Температура (°C)');
  buildLabelArea('Вероятен валеж (мм/3ч)');
  buildLabelArea('Облачност (%)');
  buildLabelArea('Рейтинг на прогнозата');
};

const buildHourlyDataColumn = (
    hour,
    wind,
    windGuts,
    windDirection,
    temperature,
    chanceOfRain,
    clouds
) => {
  const $newDiv = $('<div></div>')
      .addClass('hourly-data-wrapper')
      .append(buildASquareField(hour, 'hour-field-formatter'))
      .append(buildASquareField(wind, 'wind-speed-formatter'))
      .append(buildASquareField(windGuts, 'wind-speed-formatter'))
      .append(buildASquareField(windDirection, 'compass'))
      .append(buildASquareField(temperature, 'temperature-field-backgroundColor'))
      .append(buildASquareField(chanceOfRain, ''))
      .append(
          buildASquareField(
              ``,
              `${weatherIconsinitialization(clouds, chanceOfRain)}`
          )
      )
      .append(buildASquareField(``, `${forecastRatingBuilder(wind)}`));
  $newDiv.appendTo('.all-forecast-data-wrapper');
};

const buildLoaderDiv = () => {
  $('<div></div>')
      .addClass('loader')
      .appendTo('.all-forecast-data-wrapper');
};

const buildForecastTable = () => {
  createDailyForecastDataWrapper();
  buildLabelDataColumn();
  buildWrapperForAllForecastData();
};

const windDirectionArrow = (compass) => {
  compass.forEach((windDirection) => {
    const direction = windDirection.textContent.toLowerCase();
    windDirection.textContent = '';

    const divText = document.createElement('div');
    const paragraph = document.createElement('p');
    const text = document.createTextNode(`${direction}`);
    const arrow = document.createElement('div');

    paragraph.appendChild(text);
    divText.className = 'direction';
    divText.appendChild(paragraph);
    arrow.className = `arrow ${direction}`;

    windDirection.appendChild(divText);
    windDirection.appendChild(arrow);
  });
};

const writeDayButtonsContent = function() {
  const days = getDates();
  days[0] = 'Днес';
  days[1] = 'Утре';
  let index = 0;

  $('.nav-link').each(function() {
    $(this).text(days[index]);
    index++;
  });
};

export {
  buildHourlyDataColumn,
  buildLoaderDiv,
  createDailyForecastDataWrapper,
  buildForecastTable,
  buildWrapperForAllForecastData,
  windDirectionArrow,
  writeDayButtonsContent,
};
