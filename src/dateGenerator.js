const getDates = () => {
  const today = new Date();
  const datesArr = [];
  let formatedDate;

  formatedDate = `${today.getDate()}.${today.getMonth() + 1}`;

  datesArr.push(formatedDate);

  for (let i = 0; i < 6; i++) {
    today.setDate(today.getDate() + 1);
    formatedDate = `${today.getDate()}.${today.getMonth() + 1}`;
    datesArr.push(formatedDate);
  }

  return datesArr;
};

export { getDates };