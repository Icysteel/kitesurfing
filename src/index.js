import { appendSpots } from './dropdownService.js';
import { callApi } from './apiService.js';
import { eventHandler } from './eventHandler.js';

appendSpots();
eventHandler();
callApi();
