import * as $ from 'jquery';
import { writeDayButtonsContent } from './forecastTableBuilder.js';

const dropdownMenuOnClickEventHandler = function() {
  $('.item').on('click', function() {
    if ($('.header6').is(':visible')) {
      $('.header6').hide();
    }

    if ($('.for-us').is(':visible')) {
      $('.for-us').hide();
    }

    if ($('#page-container').is(':hidden')) {
      $('#page-container').show();
    }

    if ($('.dropdown-navbar').is(':hidden')) {
      $('.dropdown-navbar').show();
    }

    if ($('#home-tab').html() === '') {
      writeDayButtonsContent();
    }

    if ($('#home-tab')) {
      $('.daily-forecast-data-wrapper').remove();
    }

    $('.nav-link').each(function() {
      this.className = '';
      this.className = 'nav-link';
    });

    $('#home-tab').addClass('active show');
  });
};

export { dropdownMenuOnClickEventHandler };
