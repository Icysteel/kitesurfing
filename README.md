# WIND EXPLORER by Team7

# ABOUT WIND EXPLORER
One page application made for both beginners and experienced kitesurfers who want to be up-to-date with the upcoming wind forecast on the most popular kitesurfing spots in Bulgaria

## APP FEATURES
### Forecast range
- 7 days ahead detailed wind forecast

### Forecast details
  - Wind speed & guts
  - Wind direction
  - Air temperature
  - Chance of rain
  - Forecast rating 

## Team details
### Team name: 
- Team7
### Team members: 
- Rosen Ivanov (Username in telerikacademy.com - rosen.ivanov)
- Miroslav Vaklinov (Username in telerikacademy.com - Icysteel)
- Petar Todorov (Username in telerikacademy.com - petar.ptodorov)
## Gitlab repository 
- Link: https://gitlab.com/Icysteel/kitesurfing.git
## USED ASSETS IN THE PROJECT
- jQuery
- ESLINT
- Webpack + Babel
- GitLab
- Javasctipt Modules
- CSS & Bootstrap